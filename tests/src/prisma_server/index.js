const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('./generated/prisma-client')
const { resolvers } = require('./resolvers')
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

async function main() {
  const app = express();
  app.use(cors());
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  const server = new GraphQLServer({
    typeDefs: 'schema.graphql',
    resolvers,
    context: request => {
      return {
        ...request,
        prisma,
      }
    },
  })

    
  server.start({port:4010},() => console.log(`😄 Server running at http://localhost:4010`))
}
main();

  