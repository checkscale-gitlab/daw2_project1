const Game = {
    player: ({ id }, args, context) => {
      return context.prisma.newGame({ id }).player()
    },
    question: ({ id }, args, context) => {
      return context.prisma.newGame({ id }).question()
    },
    answer2: ({ id }, args, context) => {
        return context.prisma.newGame({ id }).answer2()
    }
  }

  module.exports = {
    Game,
  }