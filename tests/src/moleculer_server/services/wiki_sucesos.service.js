'use strict'

const ApiGateway = require('moleculer-web')

module.exports = {
  name: 'wiki_sucesos',
  mixins: [ApiGateway],

  // More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
  settings: {
    port: process.env.PORT || 3010,

    routes: [
      {
        path: '/wiki_sucesos',
        whitelist: [
          // Access to any actions in all services under "/api" URL
          '**'
        ],
        aliases: {
          'GET sucesos/:era': 'sucesos.get_by_era',
          /* 'GET sucesos': 'sucesos.getAll', */
        },
        cors: true,
      }
    ],

   
  }
}
