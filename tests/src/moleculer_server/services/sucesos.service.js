'use strict'

const DbService = require("../mixins/db.mixin");

module.exports = {
  name: 'sucesos',
  mixins: [DbService("sucesos")],

  settings: {
    fields: ['id', 'name','era','description',"image"]
  },

  actions: {
	
	get_by_era: {
		params: {
		  era: 'string'
		},
		handler(ctx) {
				  console.log('****ctx.params get*****');
				  console.log(ctx.params);
		  
				  return this.adapter.find({era:`${ctx.params}`})
					  .then(entity => {
						  console.log('****entity get*****');
						  console.log(entity);
						  return entity;
					  })
			  }
	  },	

    /* getAll: {
      handler(ctx) {
				console.log('****ctx.params*****');
				console.log(ctx.params);

				return this.adapter.find()
					.then(entity => {
						console.log('****entity*****');
						console.log(entity);
						return entity;
					})
			}
    }, */
  },

  methods: {
    findByID(id) {
	  return this.adapter.findOne({ "_id": id });
	},
	findByEra(era) {
		return this.adapter.findOne({ "era": era });
	  },
    
		seedDB() {
			this.logger.info("Seed Items DB...");
			return Promise.resolve()
			.then(() => this.adapter.insert({
				name:"Construcción de la Forja Estelar por el Imperio Infinito",
				era:"1",
				description:"La Fragua Estelar fue una inmensa fábrica autosuficiente creada por una de las primeras especies de la galaxia, los rakata o también llamados Constructores. Estos realizaron una búsqueda de fuentes de energía eficientes y sin saber o interesarse en lo que significaba la Fuerza, la descubrieron y estudiaron desde un punto de vista científico. La Fragua Estelar es un astillero gigante automatizado, diseñado para crear el ejército más poderoso de todos los tiempos, construido por el Imperio Infinito rakata en 30.000 ABY, a cinco mil años antes del surgimiento de la República Galáctica. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/4/49/StarForge.jpeg/revision/latest/scale-to-width-down/235?cb=20080914005255"
			}))
			.then(() => this.adapter.insert({
				name:"Nacimiento de la orden Jedi",
				era:"1",
				description:"La Orden Jedi era una noble, religiosa orden de protectores unidos en su devoción al lado luminoso de la Fuerza. Con una historia registrada a miles de años antes del alzamiento del Imperio Galáctico, los Caballeros Jedi —destacados por sus sables de luz y habilidad natural de aprovechar los poderes de la Fuerza— colocados como guardianes de la paz y la justicia en la República Galáctica. Durante la Era de la República, el Templo Jedi en el Mundo del Núcleo Coruscant sirvió como el centro de toda la actividad Jedi en la galaxia. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/8/87/Jedi_symbol.png/revision/latest/scale-to-width-down/350?cb=20180531172659"
			}))
			.then(() => this.adapter.insert({
				name:"Nacimiento de la Primera Republica Galactica",
				era:"1",
				description:"La República nació con la firma de la Constitución Galáctica alrededor del año 25.053 ABY, durante las Guerras de Unificación. Durante ese tiempo, los humanos y los duros, aplicando ingeniería inversa de la tecnología basada en La Fuerza de los rakatas, inventaron el hiperimpulsor, permitiendo a Coruscant convertirse en la capital de la República Galáctica, estatus que mantendría durante veinticinco mil años.",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/3/38/Oldrepublic.jpg/revision/latest/scale-to-width-down/130?cb=20070529141911"
			}))
			.then(() => this.adapter.insert({
				name:"Guerras Mandalorianas",
				era:"2",
				description:"La guerra estalla en el 3.964 ABY. Mandalore el Máximo buscó crear el ejército más poderoso de la galaxia derrotando a la República y sus protectores Jedi. Juntando a los clanes mandalorianos y creando a los Neo-cruzados, Mandalore atacó mundos del Borde Exterior hasta llegar a Taris, cuando fue que la República intervino. La campaña de la República fue exitosa al inicio, contando con buenos soldados como el teniente Carth Onasi. No sabían ellos que Mandalore sólo probaba la fuerza de su enemigo, para después desatar toda el poder de su armada. Los mandalorianos destruyeron las defensas de la República desde el brazo Tingel hasta el Borde Medio, y aunque la situación del República era desesperada, el Consejo Jedi rehusó participar en la lucha.Al empeorar la guerra, un grupo de Jedis rechazó el mandato del Consejo y se unió a la defensa de la República. Liderados por Revan y Malak, las fuerzas de defensa revertieron la situación y sería luego cuando Revan no solo derrotó a Mandalore en combate uno a uno, sino que concluyó la guerra al activar una superarma que destruyó el ejército mandaloriano, aunque también tomó la vida muchos soldados de la República. Posteriormente, la República retornó a su vida normal. Revan y Malak sin embargo decidieron perseguir a los mandalorianos restantes en un viaje que los llevaría hasta el Imperio Sith sellando su destino en el lado oscuro. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/1/11/MtheUvsRevan.jpg/revision/latest/scale-to-width-down/350?cb=20120116041609"
			}))
			.then(() => this.adapter.insert({
				name:"Guerra civil Jedi",
				era:"2",
				description:"Tras la finalizacion de las guerras Mandalorianas, varios Jedi, descontentos por el papel que la Republica galactica tomo en esta, se revelaron persiguiendo a los remantentes mandalorianos para darles caza. Darth Revan y Darth Malak persiguieron a sus ya derrotados enemigos hacia las profundidades del espacio. Así llegarían a Dromund Kaas, donde encontraron al renacido Imperio Sith. Lo ocurrido en ese planeta es un misterio, pero cuando ellos regresaron lo hicieron sirviendo a un nuevo maestro dando asi inicio a la guerra civil entre los jedi que apollaban a los renegados y los que servian a la republica.",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/f/ff/MalakRevanSithtroops-Timeline8.jpg/revision/latest/scale-to-width-down/350?cb=20120127004503"
			}))
			.then(() => this.adapter.insert({
				name:"Septima Batalla de Rusaan",
				era:"2",
				description:"La Séptima Batalla de Ruusan fue el enfrentamiento final del conflicto conocido como las Nuevas Guerras Sith. Hubo siete batallas en este planeta durante los últimos días de la guerra, pero la última y crucial batalla es la que más se recuerda, debido al uso de la bomba de pensamiento, y es usualmente referida sólo como la Batalla de Ruusan. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/6/68/NewSithWars.JPG/revision/latest/scale-to-width-down/350?cb=20090117233745"
			}))
			.then(() => this.adapter.insert({
				name:"Invasión de Naboo",
				era:"3",
				description:"La Invasión de Naboo, también conocida como la Crisis de Naboo, fue un conflicto galáctico fundamental que tuvo lugar en el 32 ABY, diez años antes de las Guerras Clon, que sentó las bases para la guerra misma. Durante la invasión, la ciudad capital de Naboo y las regiones circundantes fueron ocupadas por el ejército de droides de batalla de la Federación de Comercio.",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/0/0b/Trade_Federation_Droid_Army.png/revision/latest/scale-to-width-down/350?cb=20180103170509"
			}))
			.then(() => this.adapter.insert({
				name:"Guerras Clon",
				era:"3",
				description:"Las Guerras Clon, la Guerra de los Clones, ocasionalmente referida como la Guerra Clon o la Guerra Separatista, fue un conflicto de tres años que dividió la galaxia en la guerra entre la República Galáctica y la disidente Confederación de Sistemas Independientes. El Señor Oscuro de los Sith Darth Sidious diseñó la guerra para permitir el exterminio de la Orden Jedi y el ascenso del Imperio Galáctico. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/4/46/Republic_Assault-0.png/revision/latest/scale-to-width-down/350?cb=20170918164911"
			}))
			.then(() => this.adapter.insert({
				name:"Orden 66",
				era:"3",
				description:"La Orden 66 fue un suceso que tuvo lugar al final de las Guerras Clon en el que los clones del Gran Ejército de la República se volvieron en contra de sus comandantes Jedi y los ejecutaron, destruyendo de este modo a la Orden Jedi. El suceso se produjo como consecuencia del Protocolo Clon 66, una orden implantada en el cerebro de los clones por los lores Sith y los científicos kaminoanos que crearon al ejército clon.",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/4/44/End_Days.jpg/revision/latest/scale-to-width-down/350?cb=20140707102959"
			}))
			.then(() => this.adapter.insert({
				name:"Guerra Civil Galáctica",
				era:"4",
				description:"La Guerra Civil Galáctica fue una lucha por el poder galáctico de cinco años en la cual la Alianza para Restaurar la República libró una rebelión contra el Imperio Galáctico en un intento por restaurar la democracia en la galaxia. ",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/6/6f/DICE_BF_Rebeli%C3%B3n_contra_el_Imperio.jpg/revision/latest/scale-to-width-down/350?cb=20151120102816"
			}))
			.then(() => this.adapter.insert({
				name:"Batalla de Yavin",
				era:"4",
				description:"La Batalla de Yavin, también conocida como la Batalla de la Estrella de la Muerte, fue una batalla importante de la Guerra Civil Galáctica que llevó a la destrucción de la primera Estrella de la Muerte. Fue un golpe paralizante para el Imperio y una de las primeras victorias importantes de la Rebelión",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/0/00/Battle_of_Yavin_OFL.png/revision/latest/scale-to-width-down/350?cb=20190217035224"
			}))
			.then(() => this.adapter.insert({
				name:"Batalla de Hoth",
				era:"4",
				description:"La Batalla de Hoth fue una gran batalla librada en el 3 DBY y fue considerada una gran victoria para el Imperio Galáctico y la peor derrota en el campo de batalla sufrida por la Alianza Rebelde durante la Guerra Civil Galáctica. La batalla fue una invasión imperial destinada a destruir la Base Eco de la Alianza Rebelde oculta en el remoto mundo de hielo Hoth.",
				image:"https://vignette.wikia.nocookie.net/es.starwars/images/6/67/Battle_of_Hoth.jpg/revision/latest/scale-to-width-down/350?cb=20171220230348"
			}))
			
			
    	},
	},

	afterConnected() {
		return this.adapter.count().then(count => {
			if (count == 0) {
				this.seedDB();
			}
		});
	}

}
