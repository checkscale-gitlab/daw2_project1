const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const slug =require('slug');
const Users = require('./Users');
let User = mongoose.model('User');


var PlanetSchema = new mongoose.Schema({
    slug: {type: String, lowercase: true, unique: true},
    name: String,
    rotation_period: String,
    orbital_period: String,
    diameter: String,
    climate:String,
    gravity:String,
    terrain:Array,
    surface_water:String,
    population:String,
    image:String,
    favoritesCount: {type: Number, default: 0},
}, {timestamps: true});

PlanetSchema.plugin(uniqueValidator, {message: 'is already taken'});
PlanetSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }
  next();
});
PlanetSchema.methods.slugify = function() {
  this.slug = slug(this.name).toString(36);
};

PlanetSchema.methods.updateFavoriteCount = function() {
  var Planets = this;

  return User.count({favorites: {$in: [Planets._id]}}).then(function(count){
    Planets.favoritesCount = count;

    return Planets.save();
  });
};

PlanetSchema.methods.isFavorited = function(user) {
  return {
    favorited: user ? user.isFavorite(this._id) : false
  }
}
PlanetSchema.methods.toJSONFor = function(user){
  return {
    slug: this.slug,
    name: this.name,
    rotation_period: this.rotation_period,
    orbital_period: this.orbital_period,
    diameter: this.diameter,
    climate:this.climate,
    gravity:this.gravity,
    terrain:this.terrain,
    surface_water:this.surface_water,
    population:this.population,
    favorited: user ? user.isFavorite(this._id) : false,
    favoritesCount: this.favoritesCount,
    image:this.image,
  };
};
PlanetSchema.methods.toJsonForComents = function(){
  return{
    slug:this.slug,
    name:this.name
  }
}

mongoose.model('Planets', PlanetSchema);

 