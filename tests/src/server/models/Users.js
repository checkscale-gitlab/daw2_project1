var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var secret = require('../config').secret;


var UserSchema = new mongoose.Schema({
    username: {type: String, lowercase: true, unique: true, index: true},
    email: {type: String, lowercase: true, unique: true, required: [true, "can't be blank"], match: [/\S+@\S+\.\S+/, 'is invalid'], index: true},
    bio: String,
    image: String,
    hash: String,
    salt: String,
    idsocial:String,
    favorites: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Planets' }]
    /* following: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }], */
  }, {timestamps: true});

  /* Authentication and creation users stuff */
  UserSchema.plugin(uniqueValidator, {message: 'is already taken'});
  UserSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
  };
  UserSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  };
  UserSchema.methods.generateJWT = function() {
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    console.log("estoy aqui 1")
    return jwt.sign({
      id: this._id,
      username: this.username,
      exp: parseInt(exp.getTime() / 1000),
    }, secret);
  };
  UserSchema.methods.toAuthJSON = function(){
    return {
      username: this.username,
      email: this.email,
      token: this.generateJWT(),
      bio: this.bio,
      image: this.image || 'https://static.productionready.io/images/smiley-cyrus.jpg'
    };
  };

  /* Social actions */
  UserSchema.methods.toProfileJSONFor = function(){
    return {
      username: this.username,
      bio: this.bio,
      image: this.image || 'https://static.productionready.io/images/smiley-cyrus.jpg'
    };
  };
  UserSchema.methods.favorite = function(id){
    if(this.favorites.indexOf(id) === -1){
      this.favorites.push(id);
    }
  
    return this.save();
  };
  UserSchema.methods.unfavorite = function(id){
    this.favorites.remove(id);
    return this.save();
  };
  UserSchema.methods.isFavorite = function(id){
    return this.favorites.some(function(favoriteId){
      return favoriteId.toString() === id.toString();
    });
  };



  mongoose.model('User', UserSchema);