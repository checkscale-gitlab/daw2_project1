const router = require('express').Router();
const email = require('../../utils/send_mail');

//use the service sendgrid
router.post('/', function(req,res,next) {
  email.sendEmail(req,res);
});

module.exports = router;