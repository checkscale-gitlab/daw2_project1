function show_If_User(UserService) {
    'ngInject';
  
    return {
      restrict: 'A',
      link: function($scope, $element, $attrs) {
        $scope.UserService = UserService;

        $scope.$watch('UserService.current', function(val) {
            // If user detected
            console.log($attrs.ifUser)
            if (val) {
              if ($attrs.ifUser === val.username) {
                $element.css({ display: 'true'})
              } else {
                $element.css({ display: 'none'})
              }
            // no user detected
            }else{$element.css({ display: 'none'})}
        });
  
      }
    };
  }
  
  export default show_If_User;