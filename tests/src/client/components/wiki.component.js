class wikiCtrl {
    constructor(APIService,GraphqlService) {
        this.$onInit = function () {
            let ctrl = this
            ctrl.APIService = APIService
            ctrl.era_info=ctrl.page
            ctrl.sucesos;
            ctrl.get_sucesos(`wiki_sucesos/sucesos/${ctrl.era_info.era_id}`,ctrl.era_info.era_id)
            
        };
    }
    get_sucesos(url,era){
        let ctrl = this
        ctrl.APIService.get_from_moleculer(url,"GET","starwars2").then(function (result) {
            ctrl.sucesos = result;
         });
    }
}
let wiki = {
    bindings: { 'page': '=' },
    controller: wikiCtrl,
    template:require('./wiki.component.html')
    
}

export default wiki;