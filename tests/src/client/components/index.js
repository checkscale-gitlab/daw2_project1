import angular from 'angular';

let componentsModule = angular.module('app.components', []);


import ListErrors from './list-errors.component'
componentsModule.component('listErrors', ListErrors);

import wiki from './wiki.component'
componentsModule.component('wiki', wiki);

import product_list from './prodcut-list.component'
componentsModule.component('prductList', product_list);
import comment from './comment_component/comment.component'
componentsModule.component('comment', comment);

import ButtonList from './buttons_component/button-list.component'
componentsModule.component('buttonList', ButtonList);
import FavoriteBtn from './buttons_component/favorite.component'
componentsModule.component('favoriteBtn', FavoriteBtn);

import ShowAuthed from './ShowAuthed.directive'
componentsModule.directive("showAuthed", ShowAuthed);

import show_If_User from './ShowIfUser.directive'
componentsModule.directive("ifUser", show_If_User);

export default componentsModule;