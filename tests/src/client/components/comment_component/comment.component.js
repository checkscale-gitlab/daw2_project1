class commentCtrl {
    constructor(APIService,UserService,$state,$element,$attrs) {
        this.$onInit = function () {
            this.APIService= APIService;
            this._$state = $state;
            this._$element = $element;
            this._$attrs = $attrs;
            this.UserService = UserService;

            /* To create a coment */
            this._user={iamge:""};//user image
            this.comment = {planet:this.planet,body:""}//comment body and reference

            /* Coments from data base */
            this.comments; //all comments
            this.user_comments; //all user comments

            /* Coment actions */
            this.embed_comment={status:"",body:""}; //current user's embed comment */

            /* Diferent ways to get the coments:
                If we are in the planet details page then get all of his coments.
                If we are in the current user only profile page get the user coments
             */
            if (this.planet) {
                this.get();
            }else{
                this.get_user_comment();
            }

            if(UserService.current){
                this._user.image=UserService.current.image;
            }
        };
    }
    send(){
        let ctrl = this;
        ctrl.APIService.post_put(`planets/${ctrl.comment.planet}/comments`,ctrl.comment,"POST").then(function(result){
            if (result) {
                ctrl._$state.reload();
            }
        })
    }
    get(){
        let ctrl = this;
        ctrl.APIService.get_delete(`planets/${ctrl.comment.planet}/comments`,"GET").then(function(result){
            ctrl.comments=result;
        })
    }
    delete(comment_id){
        let ctrl = this;
        console.log(comment_id)
        ctrl.APIService.get_delete(`planets/${ctrl.comment.planet}/comments/${comment_id}`,"Delete").then(function(result){
            if (result) {
                ctrl._$state.reload();
            }
        })
    }
    update(comment){
        let ctrl = this;
/*         console.log(this._$attrs) */
        switch (comment.type) {
            case "change":
                ctrl.embed_comment.status="update";
                break;
            case "close":
                ctrl.embed_comment.status=false;
                break;
            case "update":
                console.log(ctrl.embed_comment.body)
                ctrl.APIService.post_put(`planets/${ctrl.comment.planet}/comments/${comment.id}`
                ,ctrl.embed_comment.body,"PUT")
                .then(function(result){
                    if (result) {
                        ctrl._$state.reload();
                    }
                })
                break;
        }
    }
    comment(){

    }
    get_user_comment(){
        let ctrl = this;
        ctrl.APIService.get_delete(`planets/get_comment/${ctrl.UserService.current.username}`,"GET").then(function(result){
            console.log(result[0])
            ctrl.user_comments=result;
        })
    }
   
}
let comment = {
    bindings: { 'planet': '=','user':"=" },
    controller: commentCtrl,
    controllerAs:"ctrl",
    template:require('./comments.html')
    
}

export default comment;