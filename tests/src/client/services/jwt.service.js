const App = require("../config/constants.config");
export default class JWTService {
    constructor($window) {
      'ngInject';
      this._$window = $window;
    }

    //jwt operations
    jwt_save(token) {
      this._$window.localStorage[App.AppConstants.jwtKey] = token;
    }
    jwt_get() {
      console.log(this._$window.localStorage[App.AppConstants.jwtKey])
      return this._$window.localStorage[App.AppConstants.jwtKey];
    }
    jwt_destroy() {
      this._$window.localStorage.removeItem(App.AppConstants.jwtKey);
    }
   
  
  }