import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);

import ApiService from './api.service';
servicesModule.service('APIService', ApiService);

import GraphqlService from './graphql_services/graphql.service';
servicesModule.service('GraphqlService', GraphqlService);

import JWTService from './jwt.service';
servicesModule.service('JWT', JWTService);

import UserService from './user.service';
servicesModule.service('UserService', UserService);

import ToastrService from './toastr.service';
servicesModule.service('ToastrService', ToastrService);


export default servicesModule;