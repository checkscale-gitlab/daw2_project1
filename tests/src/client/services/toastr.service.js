export default class ToastrService {
    constructor(toastr) {
      'ngInject';
      this.toastr = toastr;
    }
        create(data){
            switch (data.option) {
                case 'success':
                    this.toastr.success(data.message);
                    break;
                case 'warning':
                    this.toastr.warning(data.message);
                    break;
                case 'info':
                    this.toastr.info(data.message);
                    break;
                case 'error':
                    this.toastr.error(data.message);
                    break;
                default:
                    return false;
                    
            }
      
        }
  
  }

 
