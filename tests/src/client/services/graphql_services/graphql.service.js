const App = require("../../config/constants.config");
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { setContext } from 'apollo-link-context';
import gql from 'graphql-tag'
export default class GraphqlService {
    constructor( $http,$q) {
      'ngInject';
        this._$q = $q;
        this._prisma_client = this.createClient(App.AppConstants.prisma);
        this._client = this.createClient(App.AppConstants.graphpi);
    }
    createClient(api) {
      return new ApolloClient({
          link: createHttpLink({ uri: api }),
          cache: new InMemoryCache()
      });
    }
    get(query) {
      let deferred = this._$q.defer();
      
      this._client.query({
          query: gql(query)
      }).then(
          (res) => deferred.resolve(res.data),
          (err) => deferred.reject(err)
      );

      return deferred.promise;
    } 
    get_prisma(query) {
      let deferred = this._$q.defer();
      
      this._prisma_client.query({
          query: gql(query)
      }).then(
          (res) => deferred.resolve(res.data),
          (err) => deferred.reject(err)
      );

      return deferred.promise;
    } 
    mutation_prisma(query) {
      let deferred = this._$q.defer();
      
      this._prisma_client.mutate({
        mutation: gql(query)
      }).then(
          (res) => deferred.resolve(res.data),
          (err) => deferred.reject(err)
      );

      return deferred.promise;
    }

  }