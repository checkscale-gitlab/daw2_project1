export const prisma_server_querys = {
    login: function(data) {   
        return `
        {
            login(mail:"${data.mail}"){
              id
              mail
              name
              loged_id
            }
          }
        `
    },
    register: function(data) {
      return `
      mutation{
        create_user(mail:"${data.mail}",name:"${data.name}",loged_id:"${data.loged_id}"){
          id
          mail
          name
          loged_id
        }
      }
      `
    },
    initGame: function(data) {
      return `
      mutation{
        initGame(player:"${data.player}"){
          id
          question{
            id
            questionId
            body
            answer{
              id
              answerId
              body
            }
            categorias{
              id
              name
              description
            }
          }
          answer2{
            id
            answerId
            body
          }
          result
        }
      }
      `
    },
    winGame: function(data) {
      return `
      mutation{
        update_game(game:"${data}"){
          result
        }
      }
      `
    }
  };