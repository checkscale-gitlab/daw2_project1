const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');
module.exports = {
  plugins: [
    new HtmlWebpackPlugin({
      title: "Webpack Output", 
      filename: 'index.html',
      template: 'template_index.html',
    }),
  ],
  entry: './app.js',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve("../server/public/"),
  },
  module: {
    rules: [
     /*  { // js files loader
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }, */
      {//css loaders
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' },],
        
      },
      {//image loader
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader'
          },
        ],
      },
      {//html loader
        test: /\.html$/,
        use: ["html-loader"]
      },
      
    ]
  },
  devServer: {
    hot: true,
    port: 9999
}
};

