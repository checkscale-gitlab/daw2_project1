function settingsConfig($stateProvider,$qProvider) {
  
    $stateProvider
    .state('app.settings', {
        url: '/settings',
        controller: 'settingsCtrl as $ctrl',
        template: require('./settings.html'),
        resolve: {
          settings: function(UserService) {
            return UserService.ensureAuthIs(true)
          }
        }
      })
  };
  
  export default settingsConfig;