import angular from 'angular';

// Create the module where our functionality can attach to
let settingsModule = angular.module('app.settings', []);

// Include our UI-Router config settings
import settingsConfig from './settings.config';
settingsModule.config(settingsConfig);


// Controllers
import settingsCtrl from './settings.controller';
settingsModule.controller('settingsCtrl', settingsCtrl);



export default settingsModule;