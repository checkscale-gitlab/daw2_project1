class settingsCtrl {
    constructor(UserService,ToastrService,$state) {
        this.user_data=UserService.current;
        this.UserService = UserService;
        this.ToastrService = ToastrService;
        this._state = $state;
        this.new_user_data={bio:"",username:"",image:""}
    }
    update_user(){
        let ctrl = this;
        let update={};
        for (let key in ctrl.new_user_data) {
            if(ctrl.new_user_data[key] && ctrl.new_user_data[key] != ctrl.user_data[key] ){
                update[key]=ctrl.new_user_data[key];
            }
        }
        if (Object.keys(update).length !=0) {
            ctrl.UserService.update(update).then(function(result) {
                ctrl.ToastrService.create("succes","User Updated")
                ctrl._state.go('app.profile',{ reload: true })
            }) 
        }

    }
    delete_user(){
        let ctrl = this;
        ctrl.UserService.delete().then(function(result) {
            ctrl.ToastrService.create("succes","User Deleted")
        })
    }
    
  }
 
  export default settingsCtrl;