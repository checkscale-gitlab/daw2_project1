function ContactConfig($stateProvider,$qProvider) {
  
    $stateProvider
    .state('app.contact', {
      url: '/contact',
      controller: 'ContactCtrl',
      controllerAs: '$ctrl',
      template: require('./contact.html'),
      title: 'Contact',
      
    });
   /*  $qProvider.errorOnUnhandledRejections(false); */
  
  };
  
  export default ContactConfig;