class SocialCtrl {
    constructor(UserService, $state, $scope, ToastrService) {
      'ngInject';
  
      this._User = UserService;
      this._$state = $state;
      this._$scope = $scope;
      this._toaster = ToastrService;
  
      this._User.attemptAuth("sociallogin", null).then(
        (res) => {
          console.log(res)
          this._toaster.create('success','Successfully Logged In');
          this._$state.go('app.home');
          /* location.reload(); */
          
        },
        (err) => {
          this._toaster.create('error','Error trying to login');
          this._$state.go('app.home');
        }
      )
    }
  }
  export default SocialCtrl;