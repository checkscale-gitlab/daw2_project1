function AuthConfig($stateProvider,$qProvider) {
  
    $stateProvider
    .state('app.auth', {
        url: '/login',
        controller: 'AuthCtrl as $ctrl',
        template: require('./auth.html'),
        title: 'Sign in',
        resolve: {
          auth: function(UserService) {
            return UserService.ensureAuthIs(false);
          }
        }
      })
      .state('app.logout', {
        resolve: {
          auth: function(ToastrService,UserService,$state) {
            UserService.logout();
            ToastrService.create({option:"success",message:"See next time"})
            $state.go('app.home')
            return $state.reload();
            
          }
      }
    })
      .state('app.socialLogin', {
        url: '/social_login',
        controller: 'SocialCtrl as $ctrl',
        title: 'Sign up by Social login',
        resolve: {
          auth: function(UserService) {
            return UserService.ensureAuthIs(false);
          }
        }
    })
  };
  
  export default AuthConfig;