class AuthCtrl {
    constructor(UserService,ToastrService,$state) {
        console.log("User Controller Ok");
        this.UserService = UserService;
        this.ToastrService = ToastrService;
        this._state = $state;
        this.register_credentials = {email:"",password:"",r_password:"",username:""}
        this.login_credentials = {email:"",password:""}
        this.regex = {email:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/}
    }
    login(){
        let ctrl = this;
        console.log(ctrl.login_credentials);
        ctrl.UserService.attemptAuth("login",ctrl.login_credentials).then(function(res) {
            console.log(res);
            ctrl._state.go('app.home',{ reload: true })
        })
    }
    register(){
        let ctrl = this;
        console.log(ctrl.register_credentials)
        ctrl.UserService.attemptAuth("",ctrl.register_credentials).then(function(res) {
            console.log(res);
            if(!res.errors){
                ctrl._state.go('app.home',{ reload: true })
                ctrl.ToastrService.create({option:"success",message:"User successfully registered and logged"})
            }else{
                ctrl.ToastrService.create({option:"error",message:"This Email "+res.errors.email})
            }
            
        })
    }
  }
  /* Object { email: "cpardoca1@gmail.com", password: "aaaaaaa", r_password: "aaaaaaa", username: "aaaaaaa" }
   */
  export default AuthCtrl;