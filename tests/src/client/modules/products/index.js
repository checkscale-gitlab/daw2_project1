import angular from 'angular';

// Create the module where our functionality can attach to
let productsModule = angular.module('app.products', []);

// Include our UI-Router config settings
import PrductsConfig from './products.config';
productsModule.config(PrductsConfig);


// Controllers
import PrductsCtrl from './products.controller';
productsModule.controller('PrductsCtrl', PrductsCtrl);

import ShopsCtrl from './shops/shop.controller';
productsModule.controller('ShopsCtrl', ShopsCtrl);

import ShopProductsCtrl from './products/shop_products.controller';
productsModule.controller('ShopProductsCtrl', ShopProductsCtrl);

import SingleProductsCtrl from './products/single_product.controller';
productsModule.controller('SingleProductsCtrl', SingleProductsCtrl);


export default productsModule;
