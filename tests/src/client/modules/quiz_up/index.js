import angular from 'angular';

// Create the module where our functionality can attach to
let quizModule = angular.module('app.quiz', []);

// Include our UI-Router config settings
import QuizConfig from './quiz.config';
quizModule.config(QuizConfig);


// Controllers
import QuizCtrl from './quiz.controller';
quizModule.controller('QuizCtrl', QuizCtrl);



export default quizModule;