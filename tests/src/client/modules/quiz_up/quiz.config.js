function QuizConfig($stateProvider,$qProvider) {
  
    $stateProvider
    .state('app.quiz', {
        url: '/quiz',
        controller: 'QuizCtrl as $ctrl',
        template: require('./quiz.html'),
      })
  };
  
  export default QuizConfig;