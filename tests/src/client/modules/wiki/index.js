import angular from 'angular';

// Create the module where our functionality can attach to
let wikiModule = angular.module('app.wiki', []);

// Include our UI-Router config wiki
import wikiConfig from './wiki.config';
wikiModule.config(wikiConfig);


// Controllers
import wikiCtrl from './wiki.controller';
wikiModule.controller('wikiCtrl', wikiCtrl);



export default wikiModule;