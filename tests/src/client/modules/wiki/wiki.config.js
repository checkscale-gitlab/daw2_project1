function wikiConfig($stateProvider,$qProvider) {
  
    $stateProvider
    .state('app.wiki', {
        url: '/era/:id',
        controller: 'wikiCtrl as $ctrl',
        template: require('./wiki.html'),
        resolve: {
          wiki: function(APIService, $state, $stateParams) {
          return APIService.get_from_moleculer(`wiki_eras/eras/${$stateParams.id}`,"GET","starwars1").then(
            (wiki) => wiki,
            (err) => $state.go('app.home')
          )
        }
        }
      })
  };
  
  export default wikiConfig;