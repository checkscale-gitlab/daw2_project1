import authInterceptor from './auth.interceptor'
function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider) {
  'ngInject';
  $httpProvider.interceptors.push(authInterceptor);
  $stateProvider
  .state('app', {
    abstract: true,
    template: require('../layout/app-view.html'),
    resolve: {
      auth: function(UserService) {
        return UserService.verifyAuth();
      }
    }
  });
  $urlRouterProvider.otherwise('/');
}
export default AppConfig;
