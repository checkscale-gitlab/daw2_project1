export const AppConstants = {
    //api: 'https://conduit.productionready.io/api',
    graphpi: 'http://localhost:3999/graphql/',
    api: 'http://localhost:3002/api',
    prisma: 'http://localhost:4010',
    moleculer: function(domain) {
      return `http://${domain}.localhost:3010/`
    },
    jwtKey: 'jwtToken',
    appName: 'Conduit',
  };
  