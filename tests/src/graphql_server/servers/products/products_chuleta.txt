db.products.insert({ product_id:1,name:"Estrella de la muerte",description:"No es una luna",
image:"https://vignette.wikia.nocookie.net/starwars/images/9/9d/DSI_hdapproach.png/revision/latest/scale-to-width-down/500?cb=20130221005853"});
db.products.insert({product_id:2, name:"X Wing",description:"The most iconic wing of the resistance",
image:"https://vignette.wikia.nocookie.net/starwars/images/8/80/X-wing_Fathead.png/revision/latest/scale-to-width-down/500?cb=20161004003846"});
db.products.insert({product_id:3, name:"Y Wing",description:"Considered both, gunner and bomber",
image:"https://vignette.wikia.nocookie.net/starwars/images/4/45/BTL-B_Y-wing_BF2.png/revision/latest/scale-to-width-down/250?cb=20171129235141"});
db.products.insert({product_id:4, name:"N1 starfighter",description:"A 8 year's old kid drive this starfighter",
image:"https://vignette.wikia.nocookie.net/starwars/images/d/d3/N-1_BF2.png/revision/latest/scale-to-width-down/250?cb=20170825000654"});
db.products.insert({product_id:5, name:"Tie Silencer",description:"The most advanced version of the Tie Fighter",
image:"https://vignette.wikia.nocookie.net/starwars/images/2/22/TIE_Silencer.png/revision/latest/scale-to-width-down/250?cb=20171223075800"});
db.products.insert({product_id:6, name:"Ghost",description:"The starship used by the Spectres",
image:"https://vignette.wikia.nocookie.net/starwars/images/7/71/Ghost-Fathead.png/revision/latest/scale-to-width-down/500?cb=20161109002445"});
db.products.insert({ product_id:7,name:"Star Destroyer",description:"Iconic imperial ship",
image:"https://vignette.wikia.nocookie.net/starwars/images/2/2a/ISD-SWE.png/revision/latest?cb=20140718023302"});
db.products.insert({product_id:8, name:"Corvus",description:"The Corvus was a Raider II-class corvette used by Inferno Squad during the Galactic Civil War.",
image:"https://vignette.wikia.nocookie.net/starwars/images/4/46/Corvus_New_Republic.png/revision/latest/scale-to-width-down/500?cb=20171113215427"});
db.products.insert({ product_id:9,name:"Shadow Caster",description:"Used by the bounty hunters",
image:"https://vignette.wikia.nocookie.net/starwars/images/b/b5/Shadow_Caster_firing.png/revision/latest/scale-to-width-down/500?cb=20160319210223"});
db.products.insert({ product_id:10,name:"Ardent",description:"Service to the Rebel Alliance during the Galactic Civil War. ",
image:"https://vignette.wikia.nocookie.net/starwars/images/8/84/Mon_calamari_cruiser_eaw.jpg/revision/latest?cb=20111027183219"});
