import { ApolloServer, gql } from "apollo-server-express";
import  { buildFederatedSchema } from '@apollo/federation';
import express from "express";

const startServer = async () => {
  const app = express();

  var mongoose = require('mongoose');
  const products_model= require('./models/Products.model');
  let Products = mongoose.model('products'); 
  await mongoose.connect("mongodb://localhost:27017/StarWarsTravel_test", {
    useNewUrlParser: true
  });
  const typeDefs = gql`
    extend type Query {
      all_products: [Product],
      one_product(product_id:Int!):Product
    }

    type Product @key(fields: "product_id") {
      product_id:Int!,
      name:String
      description:String
      image:String
    }
  `;
  let all_products_array;
  Products.find().then(function(result) {/* console.log(result); */  all_products_array= result })
  const resolvers = {
    Query: {
      all_products() {
        return Products.find()
      },
      one_product: (root, {product_id}) => {
        const found = all_products_array.find(product => product.product_id == product_id);
        return found;
      },
    },
    Product: {
      __resolveReference(object) {
        /* console.log(object) */
        return Products.find({product_id:object.product_id});
      }
    },
  }
  const server = new ApolloServer({
    schema: buildFederatedSchema([
      {
        typeDefs,
        resolvers,
      },
    ]),
  });

  server.applyMiddleware({ app });

  app.listen({ port: 4001 }, () =>
    console.log(`🚀 Server ready at http://localhost:4001${server.graphqlPath}`)
  );
};

startServer();
