const mongoose = require('mongoose');

const ProductsSchema = new mongoose.Schema({
  product_id:String,
  name:String,
  description:String,
  image:String,
}, {timestamps: true});

mongoose.model('products', ProductsSchema);