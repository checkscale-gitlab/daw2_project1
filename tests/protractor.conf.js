exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['test_e2e/spec.js'],
    capabilities: {
        'browserName': 'firefox'
      }
  }