# Daw2_Project1

A Small web app created using the MEAN stack.

## Index
-   Introduction
-   Technologies


## Introduction
This web app is emulating a scenario in which a single frontend requires 
information from several backends with different technologies and methods of obtaining data.

## Technologies
![](img/Daw2-P1.png)


